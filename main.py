from commands import ListBookmarksCommand, AddBookmarkCommand, DeleteBookmarkCommand, QuitCommand, \
    CreateBookmarksTableCommand


class Option:
    def __init__(self, name, command, prep_call=None):
        self.name = name
        self.command = command
        self.prep_call = prep_call

    def choose(self):
        data = self.prep_call() if self.prep_call else None
        message = self.command.execute(data) if data else self.command.execute()
        print(message)

    def __str__(self):
        return self.name


def print_options(options_list):
    for shortcut, option in options_list.items():
        print(f'({shortcut}) {option}\n')


def option_choice_is_valid(choice, options_list):
    return choice in options_list or choice.upper() in options_list


def get_option_choice(options_list):
    choice = input('Please, choose one of this options: ')
    while not option_choice_is_valid(choice, options_list):
        print("Sorry, it's wrong input! Please, try again")
        choice = input('Please, choose one of this options: ')
    return options[choice.upper()]


if __name__ == '__main__':
    print('\t\t\t\t\t\t\tGood to see you in our beautiful app - Bark!')

    CreateBookmarksTableCommand().execute()  # Create DB if not exists

    options = {
        'A': Option('Add bookmark', AddBookmarkCommand()),
        'B': Option('Print bookmarks ordered by date', ListBookmarksCommand()),
        'T': Option('Print bookmarks ordered by title', ListBookmarksCommand(order_by='title')),
        'D': Option('Delete bookmark', DeleteBookmarkCommand()),
        'Q': Option('Quit', QuitCommand())
    }
    print_options(options)

    chosen_option = get_option_choice(options)
    chosen_option.choose()

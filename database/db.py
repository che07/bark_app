import sqlite3


class DataBaseManager:
    def __init__(self, database_filename):
        # Open connection with DB
        self.connection = sqlite3.connect(database_filename)

    def __del__(self):
        # Close connection with DB
        self.connection.close()

    def _execute(self, statement: str, values: list = None):
        # Execute command and return result in cursor
        with self.connection:
            cursor = self.connection.cursor()
            cursor.execute(statement, values or [])
            return cursor

    def create_table(self, table_name: str, columns: dict):
        # Create table if not exists and her columns
        columns_with_types = [
            f'{col_name} {data_type}' for col_name, data_type in columns.items()
        ]
        self._execute(
            f'''
            CREATE TABLE if NOT EXISTS {table_name}
            ({', '.join(columns_with_types)});
            '''
        )

    def add(self, table_name: str, data: dict):
        # Add data in DB
        placeholders = ', '.join('?' * len(data))
        column_names = ', '.join(data.keys())
        column_values = list(data.values())

        self._execute(
            f'''
            INSERT INTO {table_name}
            ({column_names})
            VALUES ({placeholders});
            ''', column_values
        )

    def delete(self, table_name: str, criteria: dict):
        # Delete entity from DB
        placeholders = [f'{column} = ?' for column in criteria.keys()]
        delete_criteria = ' AND '.join(placeholders)
        self._execute(
            f'''
            DELETE FROM {table_name}
            WHERE {delete_criteria};
            ''', list(criteria.values())
        )

    def select(self, table_name: str, criteria: dict = None, order_by: str | None = None):
        # Get data from DB
        criteria = criteria or {}
        query = f'SELECT * FROM {table_name}'
        if criteria:
            placeholders = [f'{column} = ?' for column in criteria.keys()]
            select_criteria = ' AND '.join(placeholders)
            query += f' WHERE {select_criteria}'
        if order_by:
            query += f' ORDER BY {order_by}'

        return self._execute(query, list(criteria.values()))
